<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_CN" version="2.1">
<context>
    <name>DAboutDialog</name>
    <message>
        <location filename="../widgets/daboutdialog.cpp" line="190"/>
        <source>Acknowledgements</source>
        <translation>鸣谢</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1094"/>
        <source>Version: %1</source>
        <translation>版本：%1</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1098"/>
        <source>%1 is released under %2</source>
        <translation>%1遵循%2协议发布</translation>
    </message>
</context>
<context>
    <name>DCrumbEdit</name>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="896"/>
        <source>Black</source>
        <translation>黑色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="899"/>
        <source>White</source>
        <translation>白色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="902"/>
        <source>Dark Gray</source>
        <translation>深灰色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="905"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="908"/>
        <source>Light Gray</source>
        <translation>浅灰色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="911"/>
        <source>Red</source>
        <translation>红色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="914"/>
        <source>Green</source>
        <translation>绿色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="917"/>
        <source>Blue</source>
        <translation>蓝色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="920"/>
        <source>Cyan</source>
        <translation>青色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="923"/>
        <source>Magenta</source>
        <translation>洋红色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="926"/>
        <source>Yellow</source>
        <translation>黄色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="929"/>
        <source>Dark Red</source>
        <translation>深红色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="932"/>
        <source>Dark Green</source>
        <translation>深绿色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="935"/>
        <source>Dark Blue</source>
        <translation>深蓝色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="938"/>
        <source>Dark Cyan</source>
        <translation>深青色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="941"/>
        <source>Dark Magenta</source>
        <translation>深紫红色</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="944"/>
        <source>Dark Yellow</source>
        <translation>深黄色</translation>
    </message>
</context>
<context>
    <name>DInputDialog</name>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="42"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="43"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>DKeySequenceEdit</name>
    <message>
        <location filename="../widgets/dkeysequenceedit.cpp" line="42"/>
        <source>Enter a new shortcut</source>
        <translation>请输入新的快捷键</translation>
    </message>
</context>
<context>
    <name>DLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="471"/>
        <source>Stop reading</source>
        <translation>停止朗读</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="473"/>
        <source>Text to Speech</source>
        <translation>语音朗读</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="496"/>
        <source>Translate</source>
        <translation>翻译</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="518"/>
        <source>Speech To Text</source>
        <translation>语音听写</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewDialogPrivate</name>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="215"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1117"/>
        <source>Advanced</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="238"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="239"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1508"/>
        <source>Print</source>
        <translation>打印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="255"/>
        <source>Basic</source>
        <translation>基础设置</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="270"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="284"/>
        <source>Copies</source>
        <translation>打印份数</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="303"/>
        <source>Page range</source>
        <translation>页码范围</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="305"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="306"/>
        <source>Current page</source>
        <translation>当前页</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="307"/>
        <source>Select pages</source>
        <translation>指定页面</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="322"/>
        <source>Orientation</source>
        <translation>打印方向</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="346"/>
        <source>Portrait</source>
        <translation>纵向</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="356"/>
        <source>Landscape</source>
        <translation>横向</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="384"/>
        <source>Pages</source>
        <translation>页面设置</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="394"/>
        <source>Color mode</source>
        <translation>色彩</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1478"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Color</source>
        <translation>彩色</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1517"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Grayscale</source>
        <translation>黑白</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="407"/>
        <source>Margins</source>
        <translation>页边距</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Narrow (mm)</source>
        <translation>窄 (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Normal (mm)</source>
        <translation>普通 (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Moderate (mm)</source>
        <translation>适中 (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Customize (mm)</source>
        <translation>自定义 (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="415"/>
        <source>Top</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="418"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="421"/>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="424"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="469"/>
        <source>Scaling</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="483"/>
        <source>Actual size</source>
        <translation>实际大小</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="492"/>
        <source>Scale</source>
        <translation>自定义比例</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="523"/>
        <source>Paper</source>
        <translation>纸张</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="533"/>
        <source>Paper size</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="546"/>
        <source>Print Layout</source>
        <translation>打印方式</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="557"/>
        <source>Duplex</source>
        <translation>双面打印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="570"/>
        <source>N-up printing</source>
        <translation>并列打印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>2 pages/sheet, 1×2</source>
        <translation>每页2版 1×2</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>4 pages/sheet, 2×2</source>
        <translation>每页4版 2×2</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>6 pages/sheet, 2×3</source>
        <translation>每页6版 2×3</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>9 pages/sheet, 3×3</source>
        <translation>每页9版 3×3</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>16 pages/sheet, 4×4</source>
        <translation>每页16版 4×4</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="581"/>
        <source>Layout direction</source>
        <translation>并打顺序</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="626"/>
        <source>Page Order</source>
        <translation>打印顺序</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="636"/>
        <source>Collate pages</source>
        <translation>逐份打印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="641"/>
        <source>Print pages in order</source>
        <translation>按顺序打印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Front to back</source>
        <translation>由前向后</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Back to front</source>
        <translation>由后向前</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="667"/>
        <source>Watermark</source>
        <translation>水印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="678"/>
        <source>Add watermark</source>
        <translation>添加水印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="720"/>
        <source>Text watermark</source>
        <translation>文字水印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Confidential</source>
        <translation>绝密</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Draft</source>
        <translation>草稿</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Sample</source>
        <translation>样本</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="730"/>
        <source>Input your text</source>
        <translation>请输入自定义水印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="753"/>
        <source>Picture watermark</source>
        <translation>图片水印</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="774"/>
        <source>Layout</source>
        <translation>布局</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Tile</source>
        <translation>平铺</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Center</source>
        <translation>居中</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="787"/>
        <source>Angle</source>
        <translation>倾度</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="803"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="824"/>
        <source>Transparency</source>
        <translation>透明度</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="881"/>
        <source>Print to PDF</source>
        <translation>存为PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="882"/>
        <source>Save as Image</source>
        <translation>另存为图片</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1113"/>
        <source>Collapse</source>
        <translation>收起</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1234"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1241"/>
        <source>Flip on short edge</source>
        <translation>短边翻转</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1237"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1240"/>
        <source>Flip on long edge</source>
        <translation>长边翻转</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1337"/>
        <source>Input page numbers please</source>
        <translation>请输入打印页码</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1340"/>
        <source>Maximum page number reached</source>
        <translation>超出打印范围</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1343"/>
        <source>Input English comma please</source>
        <translation>请输入英文逗号</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1346"/>
        <source>Input page numbers like this: 1,3,5-7,11-15,18,21</source>
        <translation>请输入正确格式，例：1,3,5-7,11-15,18,21</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1483"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1575"/>
        <source>For example, 1,3,5-7,11-15,18,21</source>
        <translation>可输入格式：1,3,5-7,11-15,18,21</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>Save as PDF</source>
        <translation>保存为PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>PDF file</source>
        <translation>PDF文件格式</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2099"/>
        <source>Save as image</source>
        <translation>保存为图片</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2101"/>
        <source>Images</source>
        <translation>图片文件</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewWidget</name>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1250"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1253"/>
        <source>Confidential</source>
        <translation>绝密</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1268"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1271"/>
        <source>Draft</source>
        <translation>草稿</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1286"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1289"/>
        <source>Sample</source>
        <translation>样本</translation>
    </message>
</context>
<context>
    <name>DSearchEdit</name>
    <message>
        <location filename="../widgets/dsearchedit.cpp" line="285"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>DSettingsDialog</name>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="78"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="79"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="81"/>
        <source>This shortcut conflicts with %1, click on Add to make this shortcut effective immediately</source>
        <translation>此快捷键与%1冲突，点击添加使这个快捷键立即生效</translation>
    </message>
</context>
<context>
    <name>DShortcutEdit</name>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="31"/>
        <source>Please input a new shortcut</source>
        <translation>请输入新的快捷键</translation>
    </message>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="102"/>
        <source>None</source>
        <translation>无</translation>
    </message>
</context>
<context>
    <name>DTextEdit</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="155"/>
        <source>Stop reading</source>
        <translation>停止朗读</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="157"/>
        <source>Text to Speech</source>
        <translation>语音朗读</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="180"/>
        <source>Translate</source>
        <translation>翻译</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="202"/>
        <source>Speech To Text</source>
        <translation>语音听写</translation>
    </message>
</context>
<context>
    <name>PickColorWidget</name>
    <message>
        <location filename="../widgets/dprintpickcolorwidget.cpp" line="89"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
</context>
<context>
    <name>PrintPreviewDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="454"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widgets/dsimplelistview.cpp" line="1334"/>
        <source>No search result</source>
        <translation>无搜索结果</translation>
    </message>
    <message>
        <location filename="../widgets/private/settings/content.cpp" line="330"/>
        <source>Restore Defaults</source>
        <translation>恢复默认</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="147"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
</context>
<context>
    <name>ShortcutEdit</name>
    <message>
        <location filename="../widgets/private/settings/shortcutedit.cpp" line="117"/>
        <source>Please enter a new shortcut</source>
        <translation>请输入新的快捷键</translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="526"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="528"/>
        <source>Light Theme</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="529"/>
        <source>Dark Theme</source>
        <translation>深色</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="530"/>
        <source>System Theme</source>
        <translation>跟随系统</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="554"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="561"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="568"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
</context>
</TS>