<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.1">
<context>
    <name>DAboutDialog</name>
    <message>
        <location filename="../widgets/daboutdialog.cpp" line="190"/>
        <source>Acknowledgements</source>
        <translation>Подяки</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1094"/>
        <source>Version: %1</source>
        <translation>Версія: %1</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1098"/>
        <source>%1 is released under %2</source>
        <translation>%1 випущено за умов дотримання %2</translation>
    </message>
</context>
<context>
    <name>DCrumbEdit</name>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="896"/>
        <source>Black</source>
        <translation>Чорний</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="899"/>
        <source>White</source>
        <translation>Білий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="902"/>
        <source>Dark Gray</source>
        <translation>Темно сірий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="905"/>
        <source>Gray</source>
        <translation>Сірий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="908"/>
        <source>Light Gray</source>
        <translation>Світло сірий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="911"/>
        <source>Red</source>
        <translation>Червоний</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="914"/>
        <source>Green</source>
        <translation>Зелений</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="917"/>
        <source>Blue</source>
        <translation>Синій</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="920"/>
        <source>Cyan</source>
        <translation>Бірюзовий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="923"/>
        <source>Magenta</source>
        <translation>Пурпурний</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="926"/>
        <source>Yellow</source>
        <translation>Жовтий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="929"/>
        <source>Dark Red</source>
        <translation>Темно-червоний</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="932"/>
        <source>Dark Green</source>
        <translation>Темно-зелений</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="935"/>
        <source>Dark Blue</source>
        <translation>Темно-синій</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="938"/>
        <source>Dark Cyan</source>
        <translation>Темно-бірюзовий</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="941"/>
        <source>Dark Magenta</source>
        <translation>Темно-пурпурний</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="944"/>
        <source>Dark Yellow</source>
        <translation>Темно-жовтий</translation>
    </message>
</context>
<context>
    <name>DInputDialog</name>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="42"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="43"/>
        <source>Confirm</source>
        <translation>Підтвердити</translation>
    </message>
</context>
<context>
    <name>DKeySequenceEdit</name>
    <message>
        <location filename="../widgets/dkeysequenceedit.cpp" line="42"/>
        <source>Enter a new shortcut</source>
        <translation>Введіть новий ярлик</translation>
    </message>
</context>
<context>
    <name>DLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="471"/>
        <source>Stop reading</source>
        <translation>Припинити читання</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="473"/>
        <source>Text to Speech</source>
        <translation>Озвучення тексту</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="496"/>
        <source>Translate</source>
        <translation>Перекласти</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="518"/>
        <source>Speech To Text</source>
        <translation>Промовити текст</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewDialogPrivate</name>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="215"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1117"/>
        <source>Advanced</source>
        <translation>Додатково</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="238"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="239"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1508"/>
        <source>Print</source>
        <translation>Друк</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="255"/>
        <source>Basic</source>
        <translation>Основне</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="270"/>
        <source>Printer</source>
        <translation>Принтер</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="284"/>
        <source>Copies</source>
        <translation>Копії</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="303"/>
        <source>Page range</source>
        <translation>Діапазон сторінок</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="305"/>
        <source>All</source>
        <translation>Усі</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="306"/>
        <source>Current page</source>
        <translation>Поточна сторінка</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="307"/>
        <source>Select pages</source>
        <translation>Вибрані сторінки</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="322"/>
        <source>Orientation</source>
        <translation>Орієнтація</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="346"/>
        <source>Portrait</source>
        <translation>Книжкова</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="356"/>
        <source>Landscape</source>
        <translation>Альбомна</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="384"/>
        <source>Pages</source>
        <translation>Сторінки</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="394"/>
        <source>Color mode</source>
        <translation>Режим кольорів</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1478"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Color</source>
        <translation>Кольоровий</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1517"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Grayscale</source>
        <translation>Відтінки сірого</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="407"/>
        <source>Margins</source>
        <translation>Поля</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Narrow (mm)</source>
        <translation>Вузькі (мм)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Normal (mm)</source>
        <translation>Звичайні (мм)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Moderate (mm)</source>
        <translation>Помірні (мм)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Customize (mm)</source>
        <translation>Нетипові (мм)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="415"/>
        <source>Top</source>
        <translation>Згори</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="418"/>
        <source>Left</source>
        <translation>Ліворуч</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="421"/>
        <source>Bottom</source>
        <translation>Внизу</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="424"/>
        <source>Right</source>
        <translation>Праворуч</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="469"/>
        <source>Scaling</source>
        <translation>Масштабування</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="483"/>
        <source>Actual size</source>
        <translation>Природний розмір</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="492"/>
        <source>Scale</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="523"/>
        <source>Paper</source>
        <translation>Папір</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="533"/>
        <source>Paper size</source>
        <translation>Розмір аркуша</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="546"/>
        <source>Print Layout</source>
        <translation>Компонування друку</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="557"/>
        <source>Duplex</source>
        <translation>Двобічний друк</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="570"/>
        <source>N-up printing</source>
        <translation>Друк декількох сторінок на аркуші</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>2 pages/sheet, 1×2</source>
        <translation>2 сторінки/аркуш, 1×2</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>4 pages/sheet, 2×2</source>
        <translation>4 сторінки/аркуш, 2×2</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>6 pages/sheet, 2×3</source>
        <translation>6 сторінок/аркуш, 2×3</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>9 pages/sheet, 3×3</source>
        <translation>9 сторінок/аркуш, 3×3</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>16 pages/sheet, 4×4</source>
        <translation>16 сторінок/аркуш, 4×4</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="581"/>
        <source>Layout direction</source>
        <translation>Напрямок компонування</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="626"/>
        <source>Page Order</source>
        <translation>Порядок сторінок</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="636"/>
        <source>Collate pages</source>
        <translation>Упорядкувати сторінки</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="641"/>
        <source>Print pages in order</source>
        <translation>Друкувати сторінки за порядком</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Front to back</source>
        <translation>Спереду назад</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Back to front</source>
        <translation>Задом наперед</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="667"/>
        <source>Watermark</source>
        <translation>Накладний знак</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="678"/>
        <source>Add watermark</source>
        <translation>Додати накладний знак</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="720"/>
        <source>Text watermark</source>
        <translation>Текстовий накладний знак</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Confidential</source>
        <translation>Секретно</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Draft</source>
        <translation>Чернетка</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Sample</source>
        <translation>Зразок</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Custom</source>
        <translation>Нетиповий</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="730"/>
        <source>Input your text</source>
        <translation>Введіть ваш текст</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="753"/>
        <source>Picture watermark</source>
        <translation>Накладний знак — малюнок</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="774"/>
        <source>Layout</source>
        <translation>Компонування</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Tile</source>
        <translation>Мозаїка</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Center</source>
        <translation>За центром</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="787"/>
        <source>Angle</source>
        <translation>Кут</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="803"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="824"/>
        <source>Transparency</source>
        <translation>Прозорість</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="881"/>
        <source>Print to PDF</source>
        <translation>Друкувати до PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="882"/>
        <source>Save as Image</source>
        <translation>Зберегти як зображення</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1113"/>
        <source>Collapse</source>
        <translation>Згорнути</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1234"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1241"/>
        <source>Flip on short edge</source>
        <translation>Перевернути на короткій стороні</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1237"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1240"/>
        <source>Flip on long edge</source>
        <translation>Перевернути на довгій стороні</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1337"/>
        <source>Input page numbers please</source>
        <translation>Введіть номери сторінок</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1340"/>
        <source>Maximum page number reached</source>
        <translation>Досягнуто максимального номера сторінки</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1343"/>
        <source>Input English comma please</source>
        <translation>Введіть із англійською комою</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1346"/>
        <source>Input page numbers like this: 1,3,5-7,11-15,18,21</source>
        <translation>Введіть номери сторінок, ось так: 1,3,5-7,11-15,18,21</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1483"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1575"/>
        <source>For example, 1,3,5-7,11-15,18,21</source>
        <translation>Приклад: 1,3,5-7,11-15,18,21</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>Save as PDF</source>
        <translation>Зберегти як PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>PDF file</source>
        <translation>файл PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2099"/>
        <source>Save as image</source>
        <translation>Зберегти як зображення</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2101"/>
        <source>Images</source>
        <translation>Зображення</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewWidget</name>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1250"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1253"/>
        <source>Confidential</source>
        <translation>Секретно</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1268"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1271"/>
        <source>Draft</source>
        <translation>Чернетка</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1286"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1289"/>
        <source>Sample</source>
        <translation>Зразок</translation>
    </message>
</context>
<context>
    <name>DSearchEdit</name>
    <message>
        <location filename="../widgets/dsearchedit.cpp" line="285"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
</context>
<context>
    <name>DSettingsDialog</name>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="78"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="79"/>
        <source>Replace</source>
        <translation>Замінити</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="81"/>
        <source>This shortcut conflicts with %1, click on Add to make this shortcut effective immediately</source>
        <translation>Цей ярлик конфліктує з %1, натисніть кнопку Додати, щоб негайно застосувати цей ярлик</translation>
    </message>
</context>
<context>
    <name>DShortcutEdit</name>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="31"/>
        <source>Please input a new shortcut</source>
        <translation>Будь ласка, вкажіть нове клавіатурне скорочення</translation>
    </message>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="102"/>
        <source>None</source>
        <translation>Немає</translation>
    </message>
</context>
<context>
    <name>DTextEdit</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="155"/>
        <source>Stop reading</source>
        <translation>Припинити читання</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="157"/>
        <source>Text to Speech</source>
        <translation>Озвучення тексту</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="180"/>
        <source>Translate</source>
        <translation>Перекласти</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="202"/>
        <source>Speech To Text</source>
        <translation>Промовити текст</translation>
    </message>
</context>
<context>
    <name>PickColorWidget</name>
    <message>
        <location filename="../widgets/dprintpickcolorwidget.cpp" line="89"/>
        <source>Color</source>
        <translation>Кольоровий</translation>
    </message>
</context>
<context>
    <name>PrintPreviewDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="454"/>
        <source>Select All</source>
        <translation>Вибрати усі</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widgets/dsimplelistview.cpp" line="1334"/>
        <source>No search result</source>
        <translation>Нічого не знайдено</translation>
    </message>
    <message>
        <location filename="../widgets/private/settings/content.cpp" line="330"/>
        <source>Restore Defaults</source>
        <translation>Відновити значення за замовчуванням</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="147"/>
        <source>Select All</source>
        <translation>Вибрати усі</translation>
    </message>
</context>
<context>
    <name>ShortcutEdit</name>
    <message>
        <location filename="../widgets/private/settings/shortcutedit.cpp" line="117"/>
        <source>Please enter a new shortcut</source>
        <translation>Будь ласка, введіть новий ярлик</translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="526"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="528"/>
        <source>Light Theme</source>
        <translation>Світла тема</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="529"/>
        <source>Dark Theme</source>
        <translation>Темна тема</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="530"/>
        <source>System Theme</source>
        <translation>Тема системи</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="554"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="561"/>
        <source>About</source>
        <translation>Про програму</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="568"/>
        <source>Exit</source>
        <translation>Вийти</translation>
    </message>
</context>
</TS>