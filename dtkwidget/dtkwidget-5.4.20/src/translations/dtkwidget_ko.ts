<?xml version="1.0" ?><!DOCTYPE TS><TS language="ko" version="2.1">
<context>
    <name>DAboutDialog</name>
    <message>
        <location filename="../widgets/daboutdialog.cpp" line="190"/>
        <source>Acknowledgements</source>
        <translation>감사의 말</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1094"/>
        <source>Version: %1</source>
        <translation>버전: %1</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1098"/>
        <source>%1 is released under %2</source>
        <translation>%1은(는) %2에 따라 배포됩니다</translation>
    </message>
</context>
<context>
    <name>DCrumbEdit</name>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="896"/>
        <source>Black</source>
        <translation>검은색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="899"/>
        <source>White</source>
        <translation>흰색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="902"/>
        <source>Dark Gray</source>
        <translation>진한 회색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="905"/>
        <source>Gray</source>
        <translation>회색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="908"/>
        <source>Light Gray</source>
        <translation>밝은 회색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="911"/>
        <source>Red</source>
        <translation>빨간색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="914"/>
        <source>Green</source>
        <translation>녹색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="917"/>
        <source>Blue</source>
        <translation>파란색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="920"/>
        <source>Cyan</source>
        <translation>청록색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="923"/>
        <source>Magenta</source>
        <translation>자홍색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="926"/>
        <source>Yellow</source>
        <translation>노란색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="929"/>
        <source>Dark Red</source>
        <translation>진한 빨간색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="932"/>
        <source>Dark Green</source>
        <translation>진한 녹색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="935"/>
        <source>Dark Blue</source>
        <translation>진한 파란색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="938"/>
        <source>Dark Cyan</source>
        <translation>진한 청록색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="941"/>
        <source>Dark Magenta</source>
        <translation>진한 자홍색</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="944"/>
        <source>Dark Yellow</source>
        <translation>진한 노란색</translation>
    </message>
</context>
<context>
    <name>DInputDialog</name>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="42"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="43"/>
        <source>Confirm</source>
        <translation>확인</translation>
    </message>
</context>
<context>
    <name>DKeySequenceEdit</name>
    <message>
        <location filename="../widgets/dkeysequenceedit.cpp" line="42"/>
        <source>Enter a new shortcut</source>
        <translation>새 단축키를 입력하십시오</translation>
    </message>
</context>
<context>
    <name>DLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="471"/>
        <source>Stop reading</source>
        <translation>읽기 중지</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="473"/>
        <source>Text to Speech</source>
        <translation>텍스트 음성 변환</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="496"/>
        <source>Translate</source>
        <translation>번역하기</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="518"/>
        <source>Speech To Text</source>
        <translation>음성 텍스트 변환</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewDialogPrivate</name>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="215"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1117"/>
        <source>Advanced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="238"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="239"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1508"/>
        <source>Print</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="255"/>
        <source>Basic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="270"/>
        <source>Printer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="284"/>
        <source>Copies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="303"/>
        <source>Page range</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="305"/>
        <source>All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="306"/>
        <source>Current page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="307"/>
        <source>Select pages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="322"/>
        <source>Orientation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="346"/>
        <source>Portrait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="356"/>
        <source>Landscape</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="384"/>
        <source>Pages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="394"/>
        <source>Color mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1478"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Color</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1517"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Grayscale</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="407"/>
        <source>Margins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Narrow (mm)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Normal (mm)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Moderate (mm)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Customize (mm)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="415"/>
        <source>Top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="418"/>
        <source>Left</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="421"/>
        <source>Bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="424"/>
        <source>Right</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="469"/>
        <source>Scaling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="483"/>
        <source>Actual size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="492"/>
        <source>Scale</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="523"/>
        <source>Paper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="533"/>
        <source>Paper size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="546"/>
        <source>Print Layout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="557"/>
        <source>Duplex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="570"/>
        <source>N-up printing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>2 pages/sheet, 1×2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>4 pages/sheet, 2×2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>6 pages/sheet, 2×3</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>9 pages/sheet, 3×3</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>16 pages/sheet, 4×4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="581"/>
        <source>Layout direction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="626"/>
        <source>Page Order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="636"/>
        <source>Collate pages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="641"/>
        <source>Print pages in order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Front to back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Back to front</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="667"/>
        <source>Watermark</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="678"/>
        <source>Add watermark</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="720"/>
        <source>Text watermark</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Confidential</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Draft</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Sample</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Custom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="730"/>
        <source>Input your text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="753"/>
        <source>Picture watermark</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="774"/>
        <source>Layout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Tile</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Center</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="787"/>
        <source>Angle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="803"/>
        <source>Size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="824"/>
        <source>Transparency</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="881"/>
        <source>Print to PDF</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="882"/>
        <source>Save as Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1113"/>
        <source>Collapse</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1234"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1241"/>
        <source>Flip on short edge</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1237"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1240"/>
        <source>Flip on long edge</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1337"/>
        <source>Input page numbers please</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1340"/>
        <source>Maximum page number reached</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1343"/>
        <source>Input English comma please</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1346"/>
        <source>Input page numbers like this: 1,3,5-7,11-15,18,21</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1483"/>
        <source>Save</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1575"/>
        <source>For example, 1,3,5-7,11-15,18,21</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>Save as PDF</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>PDF file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2099"/>
        <source>Save as image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2101"/>
        <source>Images</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>DPrintPreviewWidget</name>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1250"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1253"/>
        <source>Confidential</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1268"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1271"/>
        <source>Draft</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1286"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1289"/>
        <source>Sample</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>DSearchEdit</name>
    <message>
        <location filename="../widgets/dsearchedit.cpp" line="285"/>
        <source>Search</source>
        <translation>검색</translation>
    </message>
</context>
<context>
    <name>DSettingsDialog</name>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="78"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="79"/>
        <source>Replace</source>
        <translation>바꾸기</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="81"/>
        <source>This shortcut conflicts with %1, click on Add to make this shortcut effective immediately</source>
        <translation>이 단축키는 %1과 충돌합니다. 추가를 클릭하여이 단축키를 즉시 적용하십시오</translation>
    </message>
</context>
<context>
    <name>DShortcutEdit</name>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="31"/>
        <source>Please input a new shortcut</source>
        <translation>새 단축키를 입력하십시오</translation>
    </message>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="102"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
</context>
<context>
    <name>DTextEdit</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="155"/>
        <source>Stop reading</source>
        <translation>읽기 중지</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="157"/>
        <source>Text to Speech</source>
        <translation>텍스트 음성 변환</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="180"/>
        <source>Translate</source>
        <translation>번역하기</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="202"/>
        <source>Speech To Text</source>
        <translation>음성 텍스트 변환</translation>
    </message>
</context>
<context>
    <name>PickColorWidget</name>
    <message>
        <location filename="../widgets/dprintpickcolorwidget.cpp" line="89"/>
        <source>Color</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PrintPreviewDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="454"/>
        <source>Select All</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widgets/dsimplelistview.cpp" line="1334"/>
        <source>No search result</source>
        <translation>검색 결과 없음</translation>
    </message>
    <message>
        <location filename="../widgets/private/settings/content.cpp" line="330"/>
        <source>Restore Defaults</source>
        <translation>기본값 복원</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="147"/>
        <source>Select All</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ShortcutEdit</name>
    <message>
        <location filename="../widgets/private/settings/shortcutedit.cpp" line="117"/>
        <source>Please enter a new shortcut</source>
        <translation>새 단축키를 입력하십시오</translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="526"/>
        <source>Theme</source>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="528"/>
        <source>Light Theme</source>
        <translation>밝은 색상 테마</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="529"/>
        <source>Dark Theme</source>
        <translation>어두운 색상 테마</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="530"/>
        <source>System Theme</source>
        <translation>시스템 테마</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="554"/>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="561"/>
        <source>About</source>
        <translation>프로그램 정보</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="568"/>
        <source>Exit</source>
        <translation>종료</translation>
    </message>
</context>
</TS>