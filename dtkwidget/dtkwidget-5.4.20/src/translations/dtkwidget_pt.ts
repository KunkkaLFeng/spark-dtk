<?xml version="1.0" ?><!DOCTYPE TS><TS language="pt" version="2.1">
<context>
    <name>DAboutDialog</name>
    <message>
        <location filename="../widgets/daboutdialog.cpp" line="190"/>
        <source>Acknowledgements</source>
        <translation>Agradecimentos</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1094"/>
        <source>Version: %1</source>
        <translation>Versão: %1</translation>
    </message>
    <message>
        <location filename="../widgets/dapplication.cpp" line="1098"/>
        <source>%1 is released under %2</source>
        <translation>%1 é lançado sob %2</translation>
    </message>
</context>
<context>
    <name>DCrumbEdit</name>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="896"/>
        <source>Black</source>
        <translation>Preto</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="899"/>
        <source>White</source>
        <translation>Branco</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="902"/>
        <source>Dark Gray</source>
        <translation>Cinzento Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="905"/>
        <source>Gray</source>
        <translation>Cinzento</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="908"/>
        <source>Light Gray</source>
        <translation>Cinzento Claro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="911"/>
        <source>Red</source>
        <translation>Vermelho</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="914"/>
        <source>Green</source>
        <translation>Verde</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="917"/>
        <source>Blue</source>
        <translation>Azul</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="920"/>
        <source>Cyan</source>
        <translation>Ciano</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="923"/>
        <source>Magenta</source>
        <translation>Magenta</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="926"/>
        <source>Yellow</source>
        <translation>Amarelo</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="929"/>
        <source>Dark Red</source>
        <translation>Vermelho Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="932"/>
        <source>Dark Green</source>
        <translation>Verde Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="935"/>
        <source>Dark Blue</source>
        <translation>Azul Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="938"/>
        <source>Dark Cyan</source>
        <translation>Ciano Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="941"/>
        <source>Dark Magenta</source>
        <translation>Magenta Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dcrumbedit.cpp" line="944"/>
        <source>Dark Yellow</source>
        <translation>Amarelo Escuro</translation>
    </message>
</context>
<context>
    <name>DInputDialog</name>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="42"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../widgets/dinputdialog.cpp" line="43"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
</context>
<context>
    <name>DKeySequenceEdit</name>
    <message>
        <location filename="../widgets/dkeysequenceedit.cpp" line="42"/>
        <source>Enter a new shortcut</source>
        <translation>Inserir um novo atalho</translation>
    </message>
</context>
<context>
    <name>DLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="471"/>
        <source>Stop reading</source>
        <translation>Parar a leitura</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="473"/>
        <source>Text to Speech</source>
        <translation>Texto para Voz</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="496"/>
        <source>Translate</source>
        <translation>Traduzir</translation>
    </message>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="518"/>
        <source>Speech To Text</source>
        <translation>Voz para Texto</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewDialogPrivate</name>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="215"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1117"/>
        <source>Advanced</source>
        <translation>Avançado</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="238"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="239"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1508"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="255"/>
        <source>Basic</source>
        <translation>Básico</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="270"/>
        <source>Printer</source>
        <translation>Impressora</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="284"/>
        <source>Copies</source>
        <translation>Cópias</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="303"/>
        <source>Page range</source>
        <translation>Intervalo de páginas</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="305"/>
        <source>All</source>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="306"/>
        <source>Current page</source>
        <translation>Pagina atual</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="307"/>
        <source>Select pages</source>
        <translation>Selecionar páginas</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="322"/>
        <source>Orientation</source>
        <translation>Orientação</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="346"/>
        <source>Portrait</source>
        <translation>Retrato</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="356"/>
        <source>Landscape</source>
        <translation>Paisagem</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="384"/>
        <source>Pages</source>
        <translation>Páginas</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="394"/>
        <source>Color mode</source>
        <translation>Modo de cor</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1478"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="396"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1517"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1524"/>
        <source>Grayscale</source>
        <translation>Escala de cinzentos</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="407"/>
        <source>Margins</source>
        <translation>Margens</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Narrow (mm)</source>
        <translation>Estreita (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Normal (mm)</source>
        <translation>Normal (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Moderate (mm)</source>
        <translation>Moderada (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="409"/>
        <source>Customize (mm)</source>
        <translation>Personalizada (mm)</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="415"/>
        <source>Top</source>
        <translation>Superior</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="418"/>
        <source>Left</source>
        <translation>Esquerda</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="421"/>
        <source>Bottom</source>
        <translation>Inferior</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="424"/>
        <source>Right</source>
        <translation>Direita</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="469"/>
        <source>Scaling</source>
        <translation>Dimensionamento </translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="483"/>
        <source>Actual size</source>
        <translation>Tamanho atual</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="492"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="523"/>
        <source>Paper</source>
        <translation>Papel</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="533"/>
        <source>Paper size</source>
        <translation>Tamanho do papel</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="546"/>
        <source>Print Layout</source>
        <translation>Disposição da impressão</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="557"/>
        <source>Duplex</source>
        <translation>Duplo</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="570"/>
        <source>N-up printing</source>
        <translation>Impressão N-up</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>2 pages/sheet, 1×2</source>
        <translation>2 páginas/folha, 1×2</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>4 pages/sheet, 2×2</source>
        <translation>4 páginas/folha, 2×2</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>6 pages/sheet, 2×3</source>
        <translation>6 páginas/folha, 2×3</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>9 pages/sheet, 3×3</source>
        <translation>9 páginas/folha, 3×3</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="572"/>
        <source>16 pages/sheet, 4×4</source>
        <translation>16 páginas/folha, 4×4</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="581"/>
        <source>Layout direction</source>
        <translation>Direção da disposição</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="626"/>
        <source>Page Order</source>
        <translation>Ordem da página</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="636"/>
        <source>Collate pages</source>
        <translation>Agrupar páginas</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="641"/>
        <source>Print pages in order</source>
        <translation>Imprimir páginas por ordem</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Front to back</source>
        <translation>De frente para trás</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="643"/>
        <source>Back to front</source>
        <translation>De trás para a frente</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="667"/>
        <source>Watermark</source>
        <translation>Marca d&apos;água</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="678"/>
        <source>Add watermark</source>
        <translation>Adicionar marca d&apos;água</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="720"/>
        <source>Text watermark</source>
        <translation>Marca d&apos;água de texto</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Confidential</source>
        <translation>Confidencial</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Draft</source>
        <translation>Rascunho</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Sample</source>
        <translation>Exemplo</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="722"/>
        <source>Custom</source>
        <translation>Personalizada</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="730"/>
        <source>Input your text</source>
        <translation>Introduza o seu texto</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="753"/>
        <source>Picture watermark</source>
        <translation>Marca de água de imagem</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="774"/>
        <source>Layout</source>
        <translation>Disposição</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Tile</source>
        <translation>Mosaico</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="776"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="787"/>
        <source>Angle</source>
        <translation>Ângulo</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="803"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="824"/>
        <source>Transparency</source>
        <translation>Transparência</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="881"/>
        <source>Print to PDF</source>
        <translation>Imprimir para PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="882"/>
        <source>Save as Image</source>
        <translation>Guardar como imagem</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1113"/>
        <source>Collapse</source>
        <translation>Recolher</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1234"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1241"/>
        <source>Flip on short edge</source>
        <translation>Virar na margem curta</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1237"/>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1240"/>
        <source>Flip on long edge</source>
        <translation>Virar na margem comprida</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1337"/>
        <source>Input page numbers please</source>
        <translation>Introduza os números da página</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1340"/>
        <source>Maximum page number reached</source>
        <translation>Número máximo de páginas atingido</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1343"/>
        <source>Input English comma please</source>
        <translation>Introduza a vírgula inglesa</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1346"/>
        <source>Input page numbers like this: 1,3,5-7,11-15,18,21</source>
        <translation>Introduza números de páginas assim: 1,3,5-7,11-15,18,21</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1483"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="1575"/>
        <source>For example, 1,3,5-7,11-15,18,21</source>
        <translation>Por exemplo, 1,3,5-7,11-15,18,21</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>Save as PDF</source>
        <translation>Guardar como PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2061"/>
        <source>PDF file</source>
        <translation>Ficheiro PDF</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2099"/>
        <source>Save as image</source>
        <translation>Guardar como imagem</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewdialog.cpp" line="2101"/>
        <source>Images</source>
        <translation>Imagens</translation>
    </message>
</context>
<context>
    <name>DPrintPreviewWidget</name>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1250"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1253"/>
        <source>Confidential</source>
        <translation>Confidencial</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1268"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1271"/>
        <source>Draft</source>
        <translation>Rascunho</translation>
    </message>
    <message>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1286"/>
        <location filename="../widgets/dprintpreviewwidget.cpp" line="1289"/>
        <source>Sample</source>
        <translation>Exemplo</translation>
    </message>
</context>
<context>
    <name>DSearchEdit</name>
    <message>
        <location filename="../widgets/dsearchedit.cpp" line="285"/>
        <source>Search</source>
        <translation>Pesquisar</translation>
    </message>
</context>
<context>
    <name>DSettingsDialog</name>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="78"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="79"/>
        <source>Replace</source>
        <translation>Substituir</translation>
    </message>
    <message>
        <location filename="../widgets/dsettingswidgetfactory.cpp" line="81"/>
        <source>This shortcut conflicts with %1, click on Add to make this shortcut effective immediately</source>
        <translation>Esse atalho entra em conflito com %1, clique em &quot;Adicionar&quot; para aplicar este atalho imediatamente</translation>
    </message>
</context>
<context>
    <name>DShortcutEdit</name>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="31"/>
        <source>Please input a new shortcut</source>
        <translation>Introduza um novo atalho</translation>
    </message>
    <message>
        <location filename="../widgets/dshortcutedit.cpp" line="102"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
</context>
<context>
    <name>DTextEdit</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="155"/>
        <source>Stop reading</source>
        <translation>Parar a leitura</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="157"/>
        <source>Text to Speech</source>
        <translation>Texto para Voz</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="180"/>
        <source>Translate</source>
        <translation>Traduzir</translation>
    </message>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="202"/>
        <source>Speech To Text</source>
        <translation>Voz para Texto</translation>
    </message>
</context>
<context>
    <name>PickColorWidget</name>
    <message>
        <location filename="../widgets/dprintpickcolorwidget.cpp" line="89"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
</context>
<context>
    <name>PrintPreviewDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../widgets/dlineedit.cpp" line="454"/>
        <source>Select All</source>
        <translation>Selecionar tudo</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widgets/dsimplelistview.cpp" line="1334"/>
        <source>No search result</source>
        <translation>Nenhum resultado da pesquisa</translation>
    </message>
    <message>
        <location filename="../widgets/private/settings/content.cpp" line="330"/>
        <source>Restore Defaults</source>
        <translation>Restaurar Predefinições</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <location filename="../widgets/dtextedit.cpp" line="147"/>
        <source>Select All</source>
        <translation>Selecionar tudo</translation>
    </message>
</context>
<context>
    <name>ShortcutEdit</name>
    <message>
        <location filename="../widgets/private/settings/shortcutedit.cpp" line="117"/>
        <source>Please enter a new shortcut</source>
        <translation>Introduza um novo atalho</translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="526"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="528"/>
        <source>Light Theme</source>
        <translation>Tema Claro</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="529"/>
        <source>Dark Theme</source>
        <translation>Tema Escuro</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="530"/>
        <source>System Theme</source>
        <translation>Tema do Sistema</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="554"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="561"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../widgets/dtitlebar.cpp" line="568"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
</context>
</TS>