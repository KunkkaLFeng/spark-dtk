
HEADERS += \
    $$PWD/dsvgrenderer.h \
    $$PWD/dtaskbarcontrol.h \
    $$PWD/dthumbnailprovider.h

SOURCES += \
    $$PWD/dsvgrenderer.cpp \
    $$PWD/dtaskbarcontrol.cpp \
    $$PWD/dthumbnailprovider.cpp
